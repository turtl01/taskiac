## requirements
- debian 11
- user - admin
- add user admin to sudoers

## setting

environments/dev/hosts - inventory

## launch

ansible-playbook -i environments/dev/hosts playbooks/main.yml -u admin -Kk
